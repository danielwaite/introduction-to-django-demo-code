from django.shortcuts import render

from anotherdjango.models import NewsItem


def homepage(request):
    html = 'news_view.html'

    results = NewsItem.objects.all()

    return render(request, html, {'data': results})
